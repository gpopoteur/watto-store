export const setCurrentProduct = (state, { id }) => {
  state.currentProductId = id
}

export const prepareProducts = (state, { products }) => {
  state.products = products.map(product => {
    // SEO friendly
    product.id = product.name.toLowerCase().replace(/ /g, '-')
    product.media = state.media[product.id] || []

    return product
  })
}

export const showProducts = state => {
  // Lets delay the element class
  let waitTime = 100

  for (var i = 0; i < state.products.length; i++) {
    let product = state.products[i];

    ((index) => {
      setTimeout(() => {
        product = Object.assign({}, product)
        product.elClass = 'show'
        state.products.splice(index, 1, product)
      }, waitTime)

      waitTime += 150
    })(i)
  }
}

export const hideProducts = state => {
  for (var i = 0; i < state.products.length; i++) {
    let product = state.products[i]

    product = Object.assign({}, product)
    product.elClass = null
    state.products.splice(i, 1, product)
  }
}
