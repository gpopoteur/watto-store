export default {
  'twin-ion-engine-starfighter': [
    {type: 'image', src: require('../assets/twin-ion-engine-starfighter.jpeg'), thumbnail: require('../assets/twin-ion-engine-starfighter.jpeg')},
    {type: 'video', src: 'https://www.youtube.com/embed/3YbioNdgss8?controls=0&showinfo=0&rel=0&autoplay=1&loop=1&t=27s', thumbnail: 'https://i.ytimg.com/vi/3YbioNdgss8/hqdefault.jpg'}
  ],
  't-65-x-wing-starfighter': [
    {type: 'image', src: require('../assets/T-65 X-wing Starfighter.jpeg'), thumbnail: require('../assets/T-65 X-wing Starfighter.jpeg')},
    {type: 'video', src: 'https://www.youtube.com/embed/kRSE6mX0RCQ?controls=0&showinfo=0&rel=0&autoplay=1&loop=1&t=27s', thumbnail: 'https://i.ytimg.com/vi/kRSE6mX0RCQ/hqdefault.jpg'}
  ],
  'y-wing-starfighter': [
    {type: 'image', src: require('../assets/Y-wing Starfighter.jpeg'), thumbnail: require('../assets/Y-wing Starfighter.jpeg')},
    {type: 'video', src: 'https://www.youtube.com/embed/XZjsmkFmDIE?controls=0&showinfo=0&rel=0&autoplay=1&loop=1', thumbnail: 'https://i.ytimg.com/vi/XZjsmkFmDIE/hqdefault.jpg'}
  ],
  'yt-1300-light-freighter': [
    {type: 'image', src: require('../assets/YT-1300 Light Freighter.jpeg'), thumbnail: require('../assets/YT-1300 Light Freighter.jpeg')},
    {type: 'video', src: 'https://www.youtube.com/embed/94VSGBSUaCc?controls=0&showinfo=0&rel=0&autoplay=1&loop=1', thumbnail: 'https://i.ytimg.com/vi/94VSGBSUaCc/hqdefault.jpg'},
    {type: 'video', src: 'https://www.youtube.com/embed/_Jgi4V_evMA?controls=0&showinfo=0&rel=0&autoplay=1&loop=1', thumbnail: 'https://i.ytimg.com/vi/_Jgi4V_evMA/hqdefault.jpg'}
  ],
  'alpha-class-xg-1-star-wing': [
    {type: 'image', src: require('../assets/Alpha-class Xg-1 Star Wing.jpeg'), thumbnail: require('../assets/Alpha-class Xg-1 Star Wing.jpeg')}
  ],
  'lambda-class-t-4a-shuttle': [
    {type: 'image', src: require('../assets/Lambda-class T-4a shuttle.jpeg'), thumbnail: require('../assets/Lambda-class T-4a shuttle.jpeg')},
    {type: 'video', src: 'https://www.youtube.com/embed/_RQ1A7agPig?controls=0&showinfo=0&rel=0&autoplay=1&loop=1', thumbnail: 'https://i.ytimg.com/vi/_RQ1A7agPig/hqdefault.jpg'},
    {type: 'video', src: 'https://www.youtube.com/embed/jsq4JCUnkYk?controls=0&showinfo=0&rel=0&autoplay=1&loop=1', thumbnail: 'https://i.ytimg.com/vi/jsq4JCUnkYk/hqdefault.jpg'}
  ],
  'rz-1-a-wing-interceptor': [
    {type: 'image', src: require('../assets/RZ-1 A-wing interceptor.jpeg'), thumbnail: require('../assets/RZ-1 A-wing interceptor.jpeg')},
    {type: 'video', src: 'https://www.youtube.com/embed/G9T9ZB4ahXA?controls=0&showinfo=0&rel=0&autoplay=1&loop=1', thumbnail: 'https://i.ytimg.com/vi/G9T9ZB4ahXA/hqdefault.jpg'},
    {type: 'video', src: 'https://www.youtube.com/embed/xq0F9oSvvxo?controls=0&showinfo=0&rel=0&autoplay=1&loop=1', thumbnail: 'https://i.ytimg.com/vi/xq0F9oSvvxo/hqdefault.jpg'}
  ],
  'b-wing-heavy-assault-starfighter': [
    {type: 'image', src: require('../assets/B-wing heavy assault starfighter.jpeg'), thumbnail: require('../assets/B-wing heavy assault starfighter.jpeg')},
    {type: 'video', src: 'https://www.youtube.com/embed/wvIlALXd3Hs?controls=0&showinfo=0&rel=0&autoplay=1&loop=1', thumbnail: 'https://i.ytimg.com/vi/wvIlALXd3Hs/hqdefault.jpg'}
  ]
}
