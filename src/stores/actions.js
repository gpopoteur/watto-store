import axios from 'axios'

export const getProducts = ({ commit }) => {
  axios.get('http://demo7475333.mockable.io/spaceships').then(res => {
    commit('prepareProducts', { products: res.data.products })
    commit('showProducts')
  })
}
