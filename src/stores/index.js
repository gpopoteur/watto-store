import Vue from 'vue'
import Vuex from 'vuex'
import * as mutations from './mutations'
import * as getters from './getters'
import * as actions from './actions'
import media from './media'

Vue.use(Vuex)

const state = {
  media,
  products: [],
  currentProductId: null
}

export default new Vuex.Store({
  state,
  getters,
  actions,
  mutations
})
