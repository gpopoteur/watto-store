export const productChunks = state => {
  var chunks = []
  var size = 3

  console.log(state.products)
  var products = state.products.slice()
  console.log(products)
  while (products.length > 0) {
    chunks.push(products.splice(0, size))
  }

  return chunks
}

export const currentProduct = state => {
  return state.products.find(function (product) {
    return product.id === state.currentProductId
  })
}

