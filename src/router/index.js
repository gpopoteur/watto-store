import Vue from 'vue'
import Router from 'vue-router'
import Products from '@/components/Products'
import ProductView from '@/components/ProductView'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Products',
      component: Products
    },
    {
      path: '/:id',
      name: 'ProductView',
      component: ProductView
    }
  ]
})
