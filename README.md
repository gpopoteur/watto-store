# Watto's Emporium

The project was built using the Vue.js framework. Make sure you have `Webpack` installed as it relies on this to run.

## Build Setup

> If you have yarn installed, replace `npm` with `yarn`

On your terminal clone the repository and install dependencies running:
`npm install`

To build files and serve the index.html page, is better if you run the following command, the app will run on localhost:8080. This is needed as built files are meant to be served over an HTTP server.
`npm run dev`

If you have any problem or questions, please let me know

All Products View:
![Screen Shot 2017-04-24 at 7.06.12 PM.png](https://bitbucket.org/repo/ngg7zGK/images/2063917445-Screen%20Shot%202017-04-24%20at%207.06.12%20PM.png)
Single Product View:
![Screen Shot 2017-04-24 at 7.06.27 PM.png](https://bitbucket.org/repo/ngg7zGK/images/1120264080-Screen%20Shot%202017-04-24%20at%207.06.27%20PM.png)